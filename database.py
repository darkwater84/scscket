from twisted.internet import reactor
from twisted.enterprise import adbapi
import MySQLdb

# Produccion Samsung Club
db_host = "sc.c6w3418wislx.sa-east-1.rds.amazonaws.com"
db_user = "root"
db_pwd  = "ctBGmD5v28AuNyFq"
db_name = "sc"

def saveMessage(message, chatId, isAgent, msgType):
	dbpool = adbapi.ConnectionPool("MySQLdb", db_host, db_user, db_pwd, db_name)
	message = message.encode("utf-8", 'ignore')
	return dbpool.runQuery("INSERT INTO mensajes(Mensaje,Tipo,Origen,iChat,Leido) VALUES (%s, %s, %r, %s, %s)", (message, msgType, isAgent, 1,True))

def getChat(id):
	db = MySQLdb.connect(db_host, db_user, db_pwd, db_name)
	cursor = db.cursor()
	cursor.execute("SELECT iChat FROM chats where UniqueId = %s", (id,))
	chats = cursor.fetchall()

	db.close()

	if len(chats) > 0 and len(chats[0]) > 0:
		return chats[0][0]

	return None

def getChats(chats):
	found = {}

	db = MySQLdb.connect(db_host, db_user, db_pwd, db_name)
	cursor = db.cursor()

	for id in chats:
		cursor.execute("SELECT iChat FROM chats where UniqueId = %s", (id,))
		chats = cursor.fetchall()

		if len(chats) > 0 and len(chats[0]) > 0:
			found[id] = chats[0][0] # iChat
		else:
			db.close()
			return None

	db.close()

	if len(found.keys()) > 0:
		return found

	return None
