#!/usr/bin/python
from twisted.internet.protocol import Factory
from twisted.protocols.basic import LineReceiver
from twisted.python import log
from txws import WebSocketFactory

import json
import random
import database

BAD_CHAT = 404
BAD_FORMAT = 400

# Instancia de conexion, una por usuario
class ChatProtocol(LineReceiver):
	groups = []
	isAgent = False

	def __init__(self, factory):
		self.factory = factory
		self.name = None
		self.state = "REGISTER"

	# Evento heredado de Protocol
	def connectionMade(self):
		if(self.state == "REGISTER"):
			self.handle_REGISTER(createRandomName())

	def handle_REGISTER(self, name):
		if name in self.factory.users:
			name = createRandomName()

		self.name = name
		self.factory.users[name] = self
		self.state = "CHAT"
		self.groups = []

	# Evento heredado de Protocol
	def connectionLost(self, reason):
		if self.name in self.factory.users:
			del self.factory.users[self.name]

	# Evento heredado de Protocol
	def dataReceived(self, line):
		print line()

		try:
			data = json.loads(line)
		except ValueError:
			self.returnError("Bad format", BAD_FORMAT)
			return

		# Si viene chats, registrarlo a esos canales y nada mas
		if(data.has_key('chats')):
			self.validateChats(data['chats'])
			return

		# Si no viene la propiedad chat o esta vacia regresar error
		if((not data.has_key('chat')) or (not data['chat'])):
			self.returnError("No se proporciono un chat", BAD_CHAT)
			return

		# Validar el chat, contra el servidor o si ya se ha usado en sockets
		if(not self.validateChat(data['chat'])):
			self.returnError("El chat es invalido", BAD_CHAT)
			return

		# Requerir que venga el parametro booleano isAgent
		if(not data.has_key('isAgent')):
			self.returnError("Falta la propiedad 'isAgent'", BAD_FORMAT)
			return

		self.isAgent = data['isAgent']

		if(data['chat'] not in self.groups):
			self.groups.append(data['chat'])

		# Mandar el mensaje a todos los oyentes correspondientes
		self.broadcastMessage(line, data['chat'])

		# Borrar chat del cache para forzarlo a revalidar contra el servidor
		if data.has_key('type') and (data['type'] == 'status'):
			if data.has_key('message') and (data['message'] == 'cerrada'):
				try:
					del self.factory.chats[data['chat']]
					print 'chat closed ', data['chat'] # esto solo se muestra en el servidor
				except:
					print 'Chat already closed'

	def sendLog(self, log, chat = None):
		if chat != None:
			self.broadcastMessage(json.dumps({ 'log':log, 'chat': chat, 'isAgent': self.isAgent }), chat)

	def returnError(self, error, code):
		self.transport.write(json.dumps({'chats':self.groups, 'error':error, 'code': code}))
		self.transport.loseConnection()

	# Mandar mensaje a las demas conexiones que esten suscritas al mismo chat
	def broadcastMessage(self, message, chat):
		for name, protocol in self.factory.users.iteritems():
			if (protocol != self) and (chat in protocol.groups):
				protocol.transport.write(message)

	def printResult(self, result):
		print result

	def saveMessage(self, message, chat):
		database.saveMessage(message, self.factory.chats[chat], self.isAgent, 'text').addCallback(self.printResult)

	def validateChat(self, chat):
		if self.factory.chats.has_key(chat):
			return True

		iChat = database.getChat(chat)

		if iChat != None:
			self.factory.chats[chat] = iChat
			return True

		return False

	def validateChats(self, chats): # Valida chats y suscribe la conexion a ellos
		chatsToCheck = []

		for chat in chats:
			if self.factory.chats.has_key(chat):
				if(chat not in self.groups):
					self.groups.append(chat)
			else:
				chatsToCheck.append(chat)

		if len(chatsToCheck) > 0:
			checkedChats = database.getChats(chats)
			if checkedChats != None:
				self.groups += chatsToCheck
				self.factory.chats.update(checkedChats)
			else:
				return False

		return True

class ChatFactory(Factory):
	def __init__(self):
		self.users = {}
		self.chats = {}

	def buildProtocol(self, addr):
		return ChatProtocol(self)

def createRandomName():
	return ''.join(random.choice('0123456789ABCDEF') for i in range(10))
