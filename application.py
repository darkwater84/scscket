from twisted.internet import reactor
from chatserver import WebSocketFactory,ChatFactory

reactor.listenTCP(8000, WebSocketFactory(ChatFactory()))
reactor.run()	